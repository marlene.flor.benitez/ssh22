# PRACTICA 14 - ldap + phpldapadmin + ssh
- Desplega un servidor ldap i una eina gràfica d’administració anomenada phpldapadmin.
- El que es posa el ldap-php.yml, és realment el mateix que es posaria si engegessim el container amb un docker run, és a dir, els paràmetres són els mateixos. A més de vincular-los a volumns que han d'estar prèviament creats.
- **ldap-config** -- /etc/ldap/slapd.d
- **ldap-dades** -- /var/lib/ldap

## Ordre per desplegar els containers del compose file

```
docker compose -f compose.yml up -d
```
## Veure si s'han engegat

```
docker compose -f compose.yml ps
```
## Eliminar un usuari, tancar els containers i tornar-los a obrir per comprobar la permanencia de dades
> EL podem fer desde la consola i desde el phpldapadmin
```
ldapdelete -xv -D 'cn=Manager,dc=edt,dc=org' -w secret 'cn=Jordi Mas,ou=usuaris,dc=edt,dc=org'
```
```
ldapsearch -x -LLL -b 'dc=edt,dc=org'
```
- Apagar containers
```
docker compose -f compose.yml stop
```
- Matar-los
```
docker compose -f compose.yml rm
```
